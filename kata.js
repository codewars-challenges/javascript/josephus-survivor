function josephusSurvivor(n, k) {
    let a = range(1, n);
    let o = 0;
    let r = {};
    while (a.length > 1) {
        r = next(a, k, o);
        a = (r.a.length === 0) ? [a.pop()] : r.a;
        o = r.o;
    }
    return a[0];
}

function range(start, end) {
    let array = [];
    for (let i = start; i <= end; i++) {
        array.push(i);
    }
    return array;
}

function next(a1, k, o1) {
    let a2 = [];
    let o2 = 0;
    let found = false;
    for (let i = 0; i < a1.length; i++) {
        if ((i + o1 + 1) % k !== 0) {
            a2.push(a1[i]);
            o2++;
        } else {
            o2 = 0;
            found = true;
        }
    }
    return {a: a2, o: found ? o2 : o2 + o1}
}

console.log(josephusSurvivor(7, 3));
console.log(josephusSurvivor(11, 19));
console.log(josephusSurvivor(1, 300));
console.log(josephusSurvivor(14, 2));
console.log(josephusSurvivor(100, 1));
